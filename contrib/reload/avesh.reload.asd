(defpackage #:avesh.reload
  (:use #:cl #:avesh))

(in-package #:avesh.reload)

(asdf:defsystem #:avesh.reload
  :description "Reload plugin for avesh"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:avesh)
  :components ((:file "reload")))
