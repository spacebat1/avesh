(defpackage #:avesh.time-command
  (:use #:cl #:avesh))

(in-package #:avesh.time-command)

(asdf:defsystem #:avesh.time-command
  :description "Time-Command plugin for avesh"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:avesh)
  :components ((:file "time-command")))
