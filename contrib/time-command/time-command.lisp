(in-package #:avesh.time-command)

(defhook time-command (:weight -1000) (command arguments next)
  (multiple-value-bind (seconds minutes hours)
      (get-decoded-time)
    (format t "[~2,'0D:~2,'0D:~2,'0D]~%" hours minutes seconds))
  (funcall next command arguments))
