(in-package #:avesh.expansion)

(defhook expansion (:weight -100) (command arguments next)
  "Expands globs suchs as ~ and *"
  (funcall next
           command
           (mapcar (lambda (arg)
                     (let ((expansion
                            (directory
                             (if (string= arg "~")
                                 ;; A very special case: ~ should be
                                 ;; ~/ for (directory) to work.
                                 "~/"
                                 arg))))
                       (if expansion
                           (format nil "~{~A~^ ~}"
                                   (mapcar #'namestring expansion))
                           arg)))
                   arguments)))
