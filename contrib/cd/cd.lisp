(in-package #:avesh.cd)

(defcommandhook cd () (command arguments next)
  (when (> (length arguments) 1)
    (error "Too many arguments to cd."))
  (let* ((oldpwd (namestring (uiop:getcwd)))
         (newpwd
          (cond
            ((= (length arguments) 0) (user-homedir-pathname))
            ((string= (first arguments) "-") (if (uiop:getenv "OLDPWD")
                                                 (uiop:getenv "OLDPWD")
                                                 ;; Don't move if we don't have OLDPWD
                                                 (oldpwd)))
            (t (first arguments)))))
    (uiop:chdir newpwd)
    (setf (uiop:getenv "PWD") (namestring newpwd))
    (setf (uiop:getenv "OLDPWD") oldpwd)))
