(defpackage #:avesh.cd
  (:use #:cl #:avesh))

(in-package #:avesh.cd)

(asdf:defsystem #:avesh.cd
  :description "Cd plugin for avesh"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:avesh)
  :components ((:file "cd")))
