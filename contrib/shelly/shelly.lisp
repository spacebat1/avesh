(in-package #:avesh.shelly)

(defhook shelly () (command arguments next)
  (unless (string= command "#")
    (return-from shelly (funcall next command arguments)))
  (when (= (length arguments) 0)
    (error "Not enough arguments to shelly."))
  ;; Totally likely to break any time.
  (shelly.core::interpret arguments))
