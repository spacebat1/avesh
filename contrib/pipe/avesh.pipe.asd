(defpackage #:avesh.pipe
  (:use #:cl #:avesh))

(in-package #:avesh.pipe)

(asdf:defsystem #:avesh.pipe
  :description "Pipe plugin for avesh"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:avesh)
  :components ((:file "pipe")))
