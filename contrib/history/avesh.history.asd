(defpackage #:avesh.history
  (:use #:cl #:avesh))

(in-package #:avesh.history)

(asdf:defsystem #:avesh.history
  :description "History plugin for avesh"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:avesh)
  :components ((:file "history")))
