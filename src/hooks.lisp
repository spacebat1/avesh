(in-package #:avesh)

(defvar *hooks* (make-hash-table))

(export 'defhook)
(defmacro defhook (name options vars &body body)
  `(progn
     (defun ,name ,vars
       ,@body)
     (setf (gethash ',name *hooks*)
           (list :function (function ,name)
                 :weight ,(or (getf options :weight) 0)
                 :type ,(or (getf options :type) :command)))))

(export 'defcommandhook)
(defmacro defcommandhook (name options vars &body body)
  `(defhook ,name ,options ,vars
     (unless (string= ,(first vars) (string-downcase (symbol-name ',name)))
       (return-from ,name (funcall ,(third vars) ,(first vars) ,(second vars))))
     ,@body))

(defun ordered-hooks (hooks type)
  "Returns a list of all the hooks of a type, ordered by weight."
  (hook-functions
   hooks
   (sort (remove-if-not (lambda (hook)
                          (eq (getf (gethash hook hooks) :type) type))
                        (hash-table-keys hooks))
         (lambda (h1 h2)
           (< (getf (gethash h1 hooks) :weight)
              (getf (gethash h2 hooks) :weight))))))

(defun hash-table-keys (hash-table)
  (let ((keys nil))
    (maphash (lambda (k v)
               (declare (ignore v))
               (push k keys))
             hash-table)
    keys))

(defun hook-functions (hooks keys)
  (mapcar (lambda (key)
            (getf (gethash key hooks) :function))
          keys))
