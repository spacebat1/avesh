(in-package #:avesh)

(defcommandhook exec () (command arguments next)
  (when (= (length arguments) 0)
    (error "Not enough arguments to exec."))
  (run-exec (format nil "~{~A~^ ~}" arguments)))

(defcommandhook exit () (command arguments next)
  (when (> (length arguments) 1)
    (error "Too many arguments to exit."))
  (uiop:quit
   (if (first arguments)
       (parse-integer (first arguments))
       0)))

(defvar *children* nil)

(defhook command (:weight 1000) (command arguments next)
  (when (> (length command) 0)
    (let ((pid (sb-posix:fork)))
      (cond
        ((<= pid -1) (error (format nil "Failed to start ~A" command)))
        ((= pid 0) (run-exec (format nil "~A ~{~A~^ ~}" command arguments)))
        (t (multiple-value-bind (pid status)
               (sb-posix:waitpid pid 0)
             (push pid *children*)
             status)))))
  (funcall next command arguments))

(cffi:defcvar *errno* :int)

(defun empty-string-p (string)
  (string= string ""))

(export 'run-exec)
(defun run-exec (command)
  (let ((parts (remove-if #'empty-string-p (uiop:split-string command))))
    (cffi:with-foreign-objects ((argv :string (1+ (length parts))))
      (dotimes (i (length parts))
        (setf (cffi:mem-aref argv :string i) (nth i parts)))
      ;; Last element must be NULL pointer
      (setf (cffi:mem-aref argv :string (length parts)) (cffi:null-pointer))
      (cffi:with-foreign-string (command (first parts))
        (when (< (cffi:foreign-funcall "execvp"
                                       :pointer command
                                       :pointer argv
                                       :int)
                 0)
          (error (cffi:foreign-funcall "strerror" :int *errno* :string)))))))
